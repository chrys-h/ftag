# ftag

ftag is a semantic file management utility. Its aim is to provide a high-level way to index and retrieve files by tags, without interfering with the already-existing hierarchical filesystem. It uses a simple command-line interface and a set theory-inspired formalism and stores its data in directory-specific SQLite databases.

## Installation

To install ftag, you will need Stack. Simply run:
```
stack build
stack install
```

## Usage

Move to your desired directory and initialize ftag with the `init` command
```
ftag init
```

Then tag files using the `tag` command:
```
ftag tag <file> <tag>
```

For instance:
```
ftag tag src/Main.hs code
```

To untag a file with a tag, use the `untag` command:
```
ftag untag src/Main.hs lisp
```

In a directory where files are already tagged, use the `ls` command to list the files tagged with a certain tag:
```
ftag ls <tag>
```

To list the files that have all the specified tags, use `ls` with many tags:
```
ftag ls "code refactor"
```

To list the files that certain tags and exclude those that have others, use a minus sign before the excluded tags:
```
ftag ls "code -refactor"
```

For more expressive power and flexibility, see *Set formalism*.

## Commands

For day-to-day use, the most important commands are:

- `ls <expr>` : list the files in the set that results from the expression.

- `tag <file> <tag>` : tags the given file with the given tag.

- `untag <file> <tag>` : untags the given file with the given tag.

- `tagm -t tag1,tag2=val,tag3 -f file1,file2,file3` : tag many files with many tags


## Set formalism

The expressions used to represent sets of files (in `ls` and `addp`) are based on set theory. Each 'set' of files can be represented:

- By the name of a tag, preset or special set

- By a comma-separated list of files enclosed in curly braces, e.g. `{Setup.hs, ftag.cabal, src/}`

These sets can be combined through three operations:

- Set union is represented by `,`

- Set intersection is represented by `+`

- Set difference is represented by `-`

For instance, listing all the files that have the tags "code" and "haskell" and `ftag.cabal` would be
```
ftag ls "code + haskell, {ftag.cabal}"
```

In order to make these expressions more convenient, certain syntactic sugar rules have been set:

- The `+` operator can be omitted when that doesn't cause ambiguity, for instance in `ftag ls "code haskell, {ftag.cabal}"`

- The special set "all", which represents the list of all konwn files, can be omitted when it doesn't cause ambiguity, for instance in `ftag ls "-haskell"` instead of `ftag ls "all - haskell"` to list all non-haskell files.

Special sets are names that always represent certain sets:

- "all" is the set of all known files

- "none" is the empty set

- "no_tags" is the set of all known files that have no tags (special tags and presets aside)


## Predicates

A certain tag on a certain file can have a value. The value can be set with:
```
ftag tag <file> <tag>=<value>
```

For instance:
```
ftag tag src/Main.hs date=20170130
```

A file with a tag with a value behaves just like a normal tagged file in normal use cases. In particular, a file is considered tagged with the tag regardless of whether or not that tag has a value. However, these values can be compared and matched using predicates in expression; a predicate is enclosed in square brackets (`[]`) and contains a comparison that can be either true or false depending on the value of the tag.
```
ftag ls "[date > 20170201]"
```

The set represented by the predicate will be considered to contain all the files that have the tag and for which the comparison holds. If the file does not have the tag or it doesn't have a value, the predicate will be considered false and the set will not contain the file. Predicate behave like normal set identifiers and can be used anywhere in expressions. For instance, to list all the files whose date is between the 1st and the 28th of Feb 2017 or do not have a date:
```
ftag ls "[date >= 20170201] [date <= 20170228], -date"
```

Note that a predicate can only contain one comparison. The allowed operators are:

- `[tag < value]`: tag is lesser than value

- `[tag <= value]`: tag is lesser than or equal to value

- `[tag = value]`: tag is equal to value

- `[tag != value]`: tag is not equal to value

- `[tag > value]`: tag is greater than value

- `[tag >= value]`: tag is greater than or equal to value

- `[tag in {val1, val2, val3, ...}]`: the value of the tag is in the given set

- `[tag ni {val1, val2, val3, ...}]`: the value of the tag is NOT in the given set

- `[tag def]`: the tag has a value

- `[tag undef]`: the tag is set but doesn't have a value


## Presets

Presets are recordings of expressions under names. Contrary to `tagall`, the expressions in presets are recomputed each time they are invoked, which makes them dynamic. Once created, a preset can be used in expressions like a tag.

To create a preset, use the command
```
preset <name> <expression>
```


## Todo

- [x] Adding/removing tags to the database
- [x] Adding/removing files to the database
    - [x] Verifying the existence of the files
- [x] Tagging/untagging files with tags
- [x] Listing files with certain tags
- [x] Set theory-like expressions as described in the README
    - [x] Data Structures
    - [x] Parsing
    - [x] Database handling
    - [x] Literal sets
    - [ ] Expression optimization ?
    - [ ] Resolve expressions with Esqueleto queries rather than direct set operations on lists
    - [x] Syntactic sugar
- [x] Refactor Main.hs
- [x] Make ftag work when .ftag.db is in a parent directory of the workig directory
    - [x] Make paths relative to the working directory rather than the location of .ftag.db
    - [ ] Tests for the path conversion system
    - [ ] CLI flag to only show the files that are in the working directory
- [x] Renaming of tags and files
- [x] Value tags and predicates
    - [x] Data structures
    - [x] Parsing
    - [x] Database support
    - [x] Merge value tags with the 'regular' tagging facilities
    - [x] CLI syntax
    - [ ] Regex-based predicate (ie. value matches the regex) or wildcard
- [x] Presets
    - [x] Data structures
    - [x] Database support
    - [x] Resolution (aka. implementing the AtomicSet typeclass)
- [x] Special tags
    - [x] `all`
    - [x] `none`
    - [x] `no_tags`
    - [ ] Special value tags ?
        - [ ] Physical size
        - [ ] MIME type
        - [ ] Date of access/creation/modification
        - [ ] UNIX permissions
        - [ ] UNIX owner/group
        - [ ] Extension
- [x] Syntax in expressions to tell special tags, presets and normal tags apart
- [ ] Command to automatically add every file in a directory and tag them with certain tags (access date, MIME type, etc.) -> Do this with a shell script ?
- [x] CLI parameters and interface
- [x] Refactor tests
- [x] Refactor the expression parser
- [x] Fix subtleties (spaces in tag names or file names, escaping characters etc.)
- [x] Refactor app/Main.hs
- [x] Mute the output of `setupDB`
- [x] Make a generic Tree structure and rewrite Expr with that
- [x] `tagall` command: add the given tags to all the results of a given expression
- [x] Sorting function: CLI option to sort the results of `ls` by the value of a given tag
- [ ] Update internal filenames when the physical files are moved or deleted (not quite sure how to do that)
- [ ] Some kind of wildcard mechanism
- [x] More robust CLI syntax (Optparse-Applicative)
    - [x] Support for commands that take lists of tags or lists of files
    - [x] Command like 'tag' that takes multiple tags and multiple files at the same time
- [x] Command `lsvals` listing all existing values of a tag
- [x] Optimize. It's really bad. Addf'ing 2000 files takes about 4'36.
- [x] Adapt the tests to the new structure
- [x] Commands to read and write the tags of a given file to a file
- [x] Allow dashes and spaces in places where dashes and spaces don't mark slices. (aka in file names)
- [ ] More graceful error management
- [x] Split the DB module into submodules
- [ ] Decide what to do about self-referential presets. Forbid them ?
- [ ] Decide how the values of presets should work
    - [ ] And then implement
- [ ] Browser/Viewer GUI


