{-# LANGUAGE FlexibleContexts, RankNTypes, OverloadedStrings #-}

module Main where

import Data.Maybe (fromMaybe)
import FTag
import Env
import Env.CLI
import Control.Monad.Trans.Class
import Control.Monad.IO.Class
import Control.Monad.Reader.Class
import qualified Data.Text as T

main :: IO ()
main = do
  opts <- parseOpts
  env <- prepareEnv opts
  runC env $ optComm $ opts

runC :: (MonadIO m) => FTEnv -> Command -> m ()
runC env CmdInit         = runFTag env FTag.init
runC env (CmdLs e s)     = runFTag env (lsExpr e $ fromMaybe [] $ T.splitOn "," <$> s) 
                           >>= mapM_ (liftIO . putStrLn . show)
runC env (CmdAddf fs)    = runFTag env $ addFile fs
runC env (CmdAddt ts)    = runFTag env $ addTag ts
runC env (CmdRmf fs)     = runFTag env $ removeFile fs
runC env (CmdRmt ts)     = runFTag env $ removeTag ts
runC env CmdLst          = runFTag env listTags >>= mapM_ (liftIO . putStrLn . show)
runC env CmdLsf          = runFTag env listFiles >>= mapM_ (liftIO . putStrLn . show)
runC env (CmdTag f t)    = runFTag env $ mTagFile [splitVal t] [f]
runC env (CmdUntag f t)  = runFTag env $ mUntagFile [t] [f]
runC env (CmdMvf o n)    = runFTag env $ renameFile o n
runC env (CmdMvt o n)    = runFTag env $ renameTag o n
runC env (CmdTagall e t) = runFTag env $ lsExpr e [] >>= mTagFile [splitVal t]
runC env (CmdLsvals t)   = runFTag env (listVals t) >>= mapM_ (liftIO . putStrLn . show)
runC env (CmdPreset n e) = runFTag env $ addPreset n e
runC env (CmdRmp n)      = runFTag env $ removePreset n
runC env (CmdTagM tt ft) = let ts = map splitVal $ T.splitOn "," tt
                               fs = map UserFileN $ T.splitOn "," ft
                            in runFTag env $ mTagFile ts fs
runC env (CmdUntagM tt ft) = let ts = T.splitOn "," tt
                                 fs = map UserFileN $ T.splitOn "," ft
                              in runFTag env $ mUntagFile ts fs
runC env (CmdWritetags f) = runFTag env (listTagsVals f) >>= mapM_ (liftIO . putStrLn . T.unpack)
runC env (CmdReadtags f) = do cs <- map (splitVal . T.pack) . lines <$> liftIO getContents
                              runFTag env $ removeAllTags f >> mTagFile cs [f]


splitVal :: T.Text -> (TagN, Maybe T.Text)
splitVal tn = case T.breakOn "=" tn of
                  (t, "") -> (t, Nothing)
                  (t, v)  -> (t, Just v)




