{-# LANGUAGE OverloadedStrings, RankNTypes, FlexibleContexts #-}

module FTag (module FTag.Data, module FTag) where

import Database.Persist
import Database.Persist.Sqlite
import Data.Text hiding (map)
import Control.Exception
import Control.Monad.IO.Class
import Data.List (nub)
import Control.Monad.Trans.Reader

import Env
import Env.Paths
import FTag.Data
import FTag.Sort
import FTag.AtomicSets
import qualified FTag.DB as DB
import FTag.DB.Errors (silentFail)
import qualified FTag.Expr as E

runFTag :: (MonadIO m) => FTEnv -> FTag a -> m a
runFTag e a = runReaderT a e

testFTag :: (MonadIO m) => [(TagN, Text, Maybe Text)] -> DBAction a -> m a
testFTag t a = runFTag testEnv $ runInDB $ DB.setupDB >> setupTest >> a
  where
    setupTest = do
      mapM_ DB.addFile $ nub $ map (\(_, fn, _) -> StorFileN fn) $ t
      mapM_ DB.addTag $ nub $ map (\(tn, _, _) -> tn) $ t
      mapM_ (\(tn, fn, v) -> DB.tagFile tn (StorFileN fn) v) t
    testEnv = FTEnv False ":memory:" ":memory:" ":memory:"

init :: FTag ()
init = runInDB DB.setupDB

-- Functions for adding and removing tags

addTag :: [TagN] -> FTag ()
addTag = runInDB . mapM_ (silentFail . DB.addTag)

removeTag :: [TagN] -> FTag ()
removeTag = runInDB . mapM_ (silentFail . DB.removeTag)

listTags :: FTag [TagN]
listTags = runInDB $ DB.listTags

renameTag :: TagN -> TagN -> FTag ()
renameTag o n = runInDB $ DB.renameTag o n

-- Functions for adding and removing files

assertFileExists :: UserFileN -> FTag ()
assertFileExists n = do
  exists <- liftIO $ pathExists $ unU n
  if exists
    then return ()
    else throw . FTNoSuchFile <$> toStor n

addFile :: [UserFileN] -> FTag ()
addFile fs = mapM_ assertFileExists fs >> mapM toStor fs >>= runInDB . mapM_ (silentFail . DB.addFile)

removeFile :: [UserFileN] -> FTag ()
removeFile fs = mapM toStor fs >>= runInDB . mapM_ (silentFail . DB.removeFile)

listFiles :: FTag [UserFileN]
listFiles = runInDB DB.listFiles >>= mapM toUser

renameFile :: UserFileN -> UserFileN -> FTag ()
renameFile o n = do
  assertFileExists n
  so <- toStor o
  sn <- toStor n
  runInDB (DB.renameFile so sn)

-- Functions for tagging and untagging

mTagFile :: [(TagN, Maybe Text)] -> [UserFileN] -> FTag ()
mTagFile ts fs = do
  fss <- mapM toStor fs
  runInDB $ mapM_ (\f -> mapM_ (\(t, v) -> silentFail $ DB.tagFile t f v) ts) fss

mUntagFile :: [TagN] -> [UserFileN] -> FTag ()
mUntagFile ts fs = do
  fss <- mapM toStor fs
  runInDB $ mapM_ (\f -> mapM_ (\t -> silentFail $ DB.untagFile t f) ts) fss

listTagsVals :: UserFileN -> FTag [Text]
listTagsVals f = do fs <- toStor f
                    map pp <$> runInDB (DB.tagsValsForFile fs)
  where pp (t, Nothing) = t
        pp (t, Just v)  = t `append` "=" `append` v

removeAllTags :: UserFileN -> FTag ()
removeAllTags f = do 
  fs <- toStor f
  runInDB $ DB.tagsForFile fs >>= mapM_ (flip DB.untagFile fs)

-- Functions to list expressions

lsExpr :: Text -> [TagN] -> FTag [UserFileN]
lsExpr e s = E.resolve (E.parse e) >>= sortByTags s >>= mapM toUser

listVals :: TagN -> FTag [Text]
listVals t = runInDB $ DB.listVals t

-- Functions to add and remove presets

addPreset :: Text -> Text -> FTag ()
addPreset n e = E.exprToStor (E.parse e) >>= runInDB . DB.addPreset n

removePreset :: Text -> FTag ()
removePreset = runInDB . DB.removePreset

