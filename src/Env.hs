{-# LANGUAGE FlexibleContexts, RankNTypes, OverloadedStrings #-}

module Env where

import Control.Monad
import Control.Monad.Reader
import Control.Monad.Reader.Class
import Control.Monad.Trans.Reader (runReaderT, ReaderT)
import Control.Monad.IO.Class
import Control.Monad.Logger
import Control.Exception (throw)
import System.IO
import System.IO.Silently (hSilence)
import Data.Maybe
import qualified Data.Text as T
import Database.Persist
import Database.Persist.Sqlite

import Env.CLI
import FTag.Data
import Env.Paths

defaultDBFile = ".ftag.db"

locateDB :: IO Path
locateDB = makeAbsolute defaultDBFile >>= recLoc
  -- FIXME: I'm pretty sure this doesn't actually work as intended.
  where recLoc p = do
          let dbFile = p `T.append` defaultDBFile
          exists <- isFile dbFile
          if exists
            then return dbFile
            else do
              let parentP = parent p
              dirExists <- isDirectory $ dropFileName parentP
              if dirExists
                then recLoc parentP
                else throw FTNoDB

prepareEnv :: FTOpts -> IO FTEnv
prepareEnv o = do
  dbPath <- case optDBPath o of
              (Just p) -> return p
              Nothing -> if optComm o == CmdInit
                           then return defaultDBFile
                           else locateDB
  absDBPath <- makeAbsolute dbPath
  currPath <- currentDir
  return $ FTEnv { envVerbose = optVerbose o
                 , envDBPath = absDBPath
                 , envDBRoot = dropFileName absDBPath
                 , envCurrentPath = currPath
                 }

-- This prints a string only if ftag is run on verbose
printV :: (MonadReader FTEnv m, MonadIO m) => String -> m ()
printV s = do verbose <- asks envVerbose
              when verbose $ liftIO $ putStrLn s

-- This prints a String to stderr, only if ftag is run on verbose
printEV :: (MonadReader FTEnv m, MonadIO m) => String -> m ()
printEV s = do verbose <- asks envVerbose
               when verbose $ liftIO $ hPutStrLn stderr s

-- Turn a path relative to the working directory to a path
-- relative to the database directory and vice-versa
toStor :: (MonadReader FTEnv m) => UserFileN -> m StorFileN
toStor f = fmap StorFileN $ reparametrize (unU f) <$> asks envCurrentPath <*> asks envDBRoot

toUser :: (MonadReader FTEnv m) => StorFileN -> m UserFileN
toUser f = fmap UserFileN $ reparametrize (unS f) <$> asks envDBRoot <*> asks envCurrentPath

instance ToStorFileN UserFileN where
  toStorFileN = toStor

-- Run an action in the database
runInDB :: DBAction a -> FTag a
runInDB a = do
  connStr <- asks envDBPath
  liftIO $ hSilence [stderr] $ runNoLoggingT $ withSqliteConn connStr $ runSqlConn a


