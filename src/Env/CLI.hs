module Env.CLI (
       Command(..), FTOpts(..), parseOpts
     ) where

import Options.Applicative
import Data.Monoid ((<>))
import qualified Data.Text as T

import Env.Paths
import FTag.Data

parseOpts = execParser $ inf $ argParser

inf = (`info` mempty)

data Command = CmdInit
             | CmdLs        T.Text    (Maybe T.Text)
             | CmdAddf      [UserFileN]
             | CmdAddt      [TagN]
             | CmdRmf       [UserFileN]
             | CmdRmt       [TagN]
             | CmdLst
             | CmdLsf
             | CmdTag       UserFileN TagN
             | CmdUntag     UserFileN TagN
             | CmdMvf       UserFileN UserFileN
             | CmdMvt       TagN      TagN
             | CmdTagall    T.Text    TagN
             | CmdLsvals    TagN
             | CmdTagM      T.Text    T.Text
             | CmdUntagM    T.Text    T.Text
             | CmdPreset    T.Text    T.Text
             | CmdRmp       T.Text
             | CmdWritetags UserFileN
             | CmdReadtags  UserFileN
             deriving (Read, Show, Eq)

data FTOpts = FTOps {
            optVerbose :: Bool 
          , optDBPath :: Maybe Path
          , optComm :: Command
          } deriving (Read, Show, Eq)

argParser = 
  FTOps <$> switch (short 'v' <> help "Show more information")
        <*> (optional (T.pack <$> (option str $ short 'd' <> long "db-path" <> metavar "FILE" 
                                     <> help "Path to the database file")))
        <*> subparser
              (command "init"      (inf $ pure CmdInit)
            <> command "ls"        (inf $ CmdLs <$> (T.pack <$> argument str (metavar "EXPRESSION"))
                                                <*> (optional (T.pack <$> (option str $ short 's' <> long "sort"))))
            <> command "addf"      (inf $ CmdAddf <$> many (UserFileN . T.pack <$> argument str (metavar "FILE")))
            <> command "addt"      (inf $ CmdAddt <$> many (T.pack <$> argument str (metavar "TAG")))
            <> command "rmf"       (inf $ CmdRmf <$> many (UserFileN . T.pack <$> argument str (metavar "FILE")))
            <> command "rmt"       (inf $ CmdRmt <$> many (T.pack <$> argument str (metavar "TAG")))
            <> command "lst"       (inf $ pure CmdLst)
            <> command "lsf"       (inf $ pure CmdLsf)
            <> command "tag"       (inf $ CmdTag <$> (UserFileN . T.pack <$> argument str (metavar "FILE"))
                                                 <*> (T.pack <$> argument str (metavar "TAG")))
            <> command "untag"     (inf $ CmdUntag <$> (UserFileN . T.pack <$> argument str (metavar "FILE"))
                                                   <*> (T.pack <$> argument str (metavar "TAG")))
            <> command "mvf"       (inf $ CmdMvf <$> (UserFileN . T.pack <$> argument str (metavar "OLD NAME"))
                                                 <*> (UserFileN . T.pack <$> argument str (metavar "NEW NAME")))
            <> command "mvt"       (inf $ CmdMvt <$> (T.pack <$> argument str (metavar "OLD NAME"))
                                                 <*> (T.pack <$> argument str (metavar "NEW NAME")))
            <> command "tagall"    (inf $ CmdTagall <$> (T.pack <$> argument str (metavar "EXPRESSION"))
                                                    <*> (T.pack <$> argument str (metavar "TAG")))
            <> command "lsvals"    (inf $ CmdLsvals <$> (T.pack <$> argument str (metavar "TAG")))
            <> command "untagm"      (inf $ CmdUntagM <$> (T.pack <$> (option str $ short 't' <> long "tags"))
                                                  <*> (T.pack <$> (option str $ short 'f' <> long "files")))
            <> command "tagm"      (inf $ CmdTagM <$> (T.pack <$> (option str $ short 't' <> long "tags"))
                                                  <*> (T.pack <$> (option str $ short 'f' <> long "files")))
            <> command "preset"    (inf $ CmdPreset <$> (T.pack <$> argument str (metavar "NAME"))
                                                    <*> (T.pack <$> argument str (metavar "EXPRESSION")))
            <> command "rmp"       (inf $ CmdRmp <$> (T.pack <$> argument str (metavar "PRESET NAME")))
            <> command "writetags" (inf $ CmdWritetags <$> (UserFileN . T.pack <$> argument str (metavar "FILE")))
            <> command "readtags"  (inf $ CmdReadtags <$> (UserFileN . T.pack <$> argument str (metavar "FILE")))
               )


