{-# LANGUAGE OverloadedStrings #-}

module Env.Paths where

import qualified Data.Text as T
import qualified System.Directory as D

type Path = T.Text

toComponents :: Path -> [T.Text]
toComponents = T.splitOn "/"

fromComponents :: [T.Text] -> Path
fromComponents = T.intercalate "/"

textify :: (String -> String) -> T.Text -> T.Text
textify f = T.pack . f . T.unpack

textifyM :: (Monad m) => (String -> m String) -> T.Text -> m T.Text
textifyM f s = T.pack <$> (f . T.unpack $ s)

addTrailingSlash :: Path -> Path
addTrailingSlash p
  | T.last p == '/' = p
  | otherwise       = T.snoc p '/'

removeTrailingE :: [Path] -> [Path]
removeTrailingE ps
  | last ps == "" = init ps
  | otherwise     = ps

isRoot :: Path -> Bool
isRoot = (== "/")

-- TODO: make this work on relative paths and paths containing '..'
parent :: Path -> Path
parent "/" = "/"
parent p = fromComponents . (++ [""]) . init . removeTrailingE . toComponents $ p

dropFileName :: Path -> Path
dropFileName p = if T.last p == '/' then p else parent p

isRelative :: Path -> Bool
isRelative = not . isAbsolute

isAbsolute :: Path -> Bool
isAbsolute = (== '/') . T.head

makeAbsolute :: Path -> IO Path
makeAbsolute = textifyM D.makeAbsolute

currentDir :: IO Path
currentDir = addTrailingSlash . T.pack <$> D.getCurrentDirectory

isFile :: Path -> IO Bool
isFile = D.doesFileExist . T.unpack

isDirectory :: Path -> IO Bool
isDirectory = D.doesDirectoryExist . T.unpack

pathExists :: Path -> IO Bool
pathExists p = (||) <$> isFile p <*> isDirectory p

reparametrize :: Path -- The path to be reparametrized
              -> Path -- The current reference point (absolute path)
              -> Path -- The new reference point (absolute)
              -> Path
reparametrize p cr nr
  | isAbsolute p = p
  | cr == nr     = p
  | otherwise    = m (toComponents p) $ r (removeTrailingE $ toComponents cr) (removeTrailingE $ toComponents nr)
  where
    -- All this was written under this assumption that paths would not have too
    -- many components. If you need to rewrite it, god help you.
    m ("..":ps) path = m ps $ init path
    m (".": ps) path = m ps path
    m p path = (addTrailingSlash $ fromComponents path) `T.append` (fromComponents p)

    r []      [] = []
    r [] (_:nrs) = ".." : r [] nrs
    r cr []      = cr
    r (c:crs) (n:nrs)
      | c == n    = r crs nrs
      | otherwise = ".." : r (c:crs) nrs



