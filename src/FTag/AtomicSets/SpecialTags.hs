{-# LANGUAGE RankNTypes, OverloadedStrings, FlexibleContexts #-}

module FTag.AtomicSets.SpecialTags where

import qualified Data.Text as T
import FTag.DB
import FTag.Data
import Env

isSpecial :: T.Text -> Bool
isSpecial = (`elem` (["all", "none"] :: [T.Text]))

resolveSpecial :: TagN -> FTag (Maybe [StorFileN])
resolveSpecial "all"     = Just <$> runInDB listFiles
resolveSpecial "none"    = return $ Just []
resolveSpecial "no_tags" = Just <$> runInDB orphanFiles
resolveSpecial _         = return Nothing

testSpecial :: TagN -> StorFileN -> FTag (Maybe FTTaggedState)
testSpecial "all"     _ = return $ Just FTTaggedNoVal
testSpecial "none"    _ = return $ Just FTNotTagged
testSpecial "no_tags" f = Just . check <$> runInDB (tagsForFile f)
  where check [] = FTTaggedNoVal
        check _  = FTNotTagged
testSpecial _         _ = return Nothing

resolveSpecialPred :: TagN -> FTPredOp -> FTag (Maybe [StorFileN])
resolveSpecialPred "all"  FTNotDefined = Just <$> runInDB listFiles
resolveSpecialPred "all"     _ = return $ Just []
resolveSpecialPred "none"    _ = return $ Just []
resolveSpecialPred "no_tags" FTNotDefined = Just <$> runInDB orphanFiles
resolveSpecialPred "no_tags" _ = return $ Just []
resolveSpecialPred _         _ = return Nothing



