{-# LANGUAGE RankNTypes, OverloadedStrings, FlexibleContexts #-}

module FTag.AtomicSets.Operations where

import Data.Maybe
import Control.Exception (throw)
import Env
import FTag.Data
import FTag.DB
import FTag.AtomicSets.SpecialTags

atomicToStor :: UserAtomicSet -> FTag StorAtomicSet
atomicToStor = runInDB . c
  where c (UASPreset  n) = SASPreset . fromJust <$> presetIDByName n
        c (UASTag     n) = SASTag . fromJust <$> tagIDByName n
        c (UASSpecial n) = return $ SASSpecial n
        c (UASByName  n) = do -- TODO: Rewrite this with Alternative
          if isSpecial n
            then return $ SASSpecial n
            else do
              mp <- presetIDByName n
              case mp of
                (Just id) -> return $ SASPreset id
                Nothing -> do
                  mt <- tagIDByName n
                  case mt of
                    (Just id) -> return $ SASTag id
                    Nothing -> throw $ FTNoSuchTag n

