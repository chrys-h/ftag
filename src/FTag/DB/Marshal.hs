{-# LANGUAGE FlexibleInstances                                            #-}

module FTag.DB.Marshal where

import Database.Persist
import Database.Persist.Sql
import Data.Int
import qualified Data.Text as T

import FTag.Data

instance PersistField StorExpr where
   toPersistValue = PersistText . T.pack . show
   fromPersistValue (PersistText s) = Right . read . T.unpack $ s

instance PersistFieldSql StorExpr where
   sqlType _ = SqlString


