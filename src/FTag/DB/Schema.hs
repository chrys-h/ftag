{-# LANGUAGE EmptyDataDecls, FlexibleContexts, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses, OverloadedStrings, RankNTypes         #-}
{-# LANGUAGE GADTs, QuasiQuotes, TypeFamilies, TemplateHaskell            #-}

module FTag.DB.Schema where

import Database.Persist
import Database.Persist.TH
import qualified Data.Text as T

import FTag.Data
import FTag.DB.Marshal

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
File
    name      T.Text
    FileUName name
    deriving Show
Tag
    name     TagN
    TagUName name
    deriving Show
Link
    tag        TagId
    file       FileId
    value      T.Text Maybe
    UniqueLink tag file
    deriving Show
Preset
    name        T.Text
    expr        StorExpr
    PresetUName name
    deriving Show
|]


