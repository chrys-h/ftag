module FTag.DB.Links where

import Database.Persist
import Database.Persist.Sql
import qualified Database.Esqueleto as E
import qualified Data.Text as T
import Control.Exception
import Control.Applicative ((<$>), (<*>))
import Control.Monad
import Data.Maybe
import Data.Int

import FTag.Data
import FTag.DB.Schema
import FTag.DB.Basic

isTagged :: TagN -> StorFileN -> DBAction FTTaggedState
isTagged t f = do
  tagID <- fmap (entityKey . fromJust) $ getBy $ TagUName t
  fileID <- fmap (entityKey . fromJust) $ getBy $ FileUName $ unS f
  res <- getBy $ UniqueLink tagID fileID
  case res of
    Nothing -> return FTNotTagged
    Just maybeVal -> case linkValue $ entityVal maybeVal of
      Nothing -> return FTTaggedNoVal
      Just v -> return $ FTTaggedWithVal v

isTaggedByID :: Int64 -> StorFileN -> DBAction (Maybe FTTaggedState)
isTaggedByID id f = tagNameByID id >>= mapM (flip isTagged f)

assertNotTagged :: TagN -> StorFileN -> DBAction ()
assertNotTagged t f = do
  tagged <- isTagged t f
  case tagged of
    FTNotTagged -> return ()
    FTTaggedNoVal -> throw $ FTFileAlreadyTagged t f Nothing
    FTTaggedWithVal v -> throw $ FTFileAlreadyTagged t f $ Just v

assertTagged :: TagN -> StorFileN -> DBAction ()
assertTagged t f = do
  tagged <- isTagged t f
  if tagged == FTNotTagged then throw $ FTFileNotTagged t f else return ()

tagFile :: TagN -> StorFileN -> Maybe T.Text -> DBAction ()
tagFile t f v = do
  -- HACK: maybe these things should be done with Esqueleto
  tagID <- fmap (entityKey . fromJust) $ getBy $ TagUName t
  fileID <- fmap (entityKey . fromJust) $ getBy $ FileUName $ unS f
  insert_ $ Link tagID fileID v

untagFile :: TagN -> StorFileN -> DBAction ()
untagFile t f = UniqueLink
                  <$> (fmap (entityKey . fromJust) $ getBy $ TagUName t)
                  <*> (fmap (entityKey . fromJust) $ getBy $ FileUName $ unS f)
                >>= deleteBy

tagsForFile :: StorFileN -> DBAction [TagN]
tagsForFile fn = do
  tags <- E.select $ E.from $ \(f, l, t) -> do
            E.where_ (f E.^. FileName E.==. E.val (unS fn))
            E.where_ (f E.^. FileId E.==. l E.^. LinkFile)
            E.where_ (t E.^. TagId E.==. l E.^. LinkTag)
            return t
  return $ map (tagName . entityVal) $ tags

tagsValsForFile :: StorFileN -> DBAction [(TagN, Maybe T.Text)]
tagsValsForFile fn = do
  tags <- E.select $ E.from $ \(f, l, t) -> do
            E.where_ (f E.^. FileName E.==. E.val (unS fn))
            E.where_ (f E.^. FileId E.==. l E.^. LinkFile)
            E.where_ (t E.^. TagId E.==. l E.^. LinkTag)
            return (t, l)
  return $ map (\(t, l) -> (tagName $ entityVal t, linkValue $ entityVal l)) $ tags

listVals :: TagN -> DBAction [T.Text]
listVals tn = do
  vals <- E.select $ E.from $ \(t, l) -> do
            E.where_ (t E.^. TagId E.==. l E.^. LinkTag)
            E.where_ (t E.^. TagName E.==. E.val tn)
            E.where_ (E.not_ $ E.isNothing $ l E.^. LinkValue)
            return l
  return $ map (fromJust . linkValue . entityVal) $ vals

filesForTag :: TagN -> DBAction [StorFileN]
filesForTag tn = do
  files <- E.select $ E.from $ \(t, l, f) -> do
             E.where_ (t E.^. TagName E.==. E.val tn)
             E.where_ (t E.^. TagId E.==. l E.^. LinkTag)
             E.where_ (f E.^. FileId E.==. l E.^. LinkFile)
             return f
  return $ map (StorFileN . fileName . entityVal) $ files

filesForTagByID :: Int64 -> DBAction (Maybe [StorFileN])
filesForTagByID id = tagNameByID id >>= mapM filesForTag

filesForPred :: TagN -> FTPredOp -> DBAction [StorFileN]
filesForPred tn po = do
    files <- E.select $ E.from $ \(t, l, f) -> do
               E.where_ (t E.^. TagName E.==. E.val tn)
               E.where_ (t E.^. TagId E.==. l E.^. LinkTag)
               E.where_ (f E.^. FileId E.==. l E.^. LinkFile)
               predWhere l po
               return f
    return $ map (StorFileN . fileName . entityVal) $ files

filesForPredByID :: Int64 -> FTPredOp -> DBAction (Maybe [StorFileN])
filesForPredByID id p = tagNameByID id >>= mapM (flip filesForPred p)

predWhere l (FTLesser      t) = E.where_ (l E.^. LinkValue E.<.  (E.just $ E.val t))
predWhere l (FTLesserOrEq  t) = E.where_ (l E.^. LinkValue E.<=. (E.just $ E.val t))
predWhere l (FTEqual       t) = E.where_ (l E.^. LinkValue E.==. (E.just $ E.val t))
predWhere l (FTNotEqual    t) = E.where_ (l E.^. LinkValue E.!=. (E.just $ E.val t))
predWhere l (FTGreater     t) = E.where_ (l E.^. LinkValue E.>.  (E.just $ E.val t))
predWhere l (FTGreaterOrEq t) = E.where_ (l E.^. LinkValue E.>=. (E.just $ E.val t))
predWhere l (FTInSet       s) = E.where_ (l E.^. LinkValue `E.in_`   (E.justList $ E.valList s))
predWhere l (FTNotInSet    s) = E.where_ (l E.^. LinkValue `E.notIn` (E.justList $ E.valList s))
predWhere l FTDefined         = E.where_ (E.not_ $ E.isNothing $ l E.^. LinkValue)
predWhere l FTNotDefined      = E.where_ (E.isNothing $ l E.^. LinkValue)

-- Returns a list of files that have no tags
orphanFiles :: DBAction [StorFileN]
orphanFiles = do
    files <- E.select $ E.from $ \f -> do
               E.where_ $ f E.^. FileId `E.notIn` (E.subList_select $ E.from $ \l -> do
                   return $ l E.^. LinkFile)
               return f
    return $ map (StorFileN . fileName . entityVal) $ files


