module FTag.DB.Errors where

import FTag.Data
import Control.Exception (SomeException)
import Control.Monad.Catch

silentFail :: DBAction () -> DBAction ()
silentFail = (`catchAll` (\_ -> return ()))

