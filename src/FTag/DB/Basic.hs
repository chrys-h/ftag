module FTag.DB.Basic where

import Database.Persist
import Database.Persist.Sql
import qualified Database.Esqueleto as E
import qualified Data.Text as T
import Control.Exception
import Control.Applicative ((<$>), (<*>))
import Control.Monad
import Data.Maybe
import Data.Int

import Env (runInDB)
import FTag.Data
import FTag.DB.Schema

setupDB :: DBAction ()
setupDB = runMigration migrateAll

-- ----------------------------------------------------------------------------
-- Operations in the Tag table
-- ----------------------------------------------------------------------------

addTag :: TagN -> DBAction ()
addTag n = insert_ (Tag n)

removeTag :: TagN -> DBAction ()
removeTag n = do
  tid <- entityKey . fromJust <$> getBy (TagUName n)
  deleteWhere [LinkTag ==. tid]
  deleteWhere [TagName ==. n]

listTags :: DBAction [TagN]
listTags = map (tagName . entityVal) <$> selectList [] [Asc TagName]

renameTag :: TagN -> TagN -> DBAction ()
renameTag o n = updateWhere [TagName ==. o] [TagName =. n]

tagIDByName :: TagN -> DBAction (Maybe Int64)
tagIDByName n = fmap (fromSqlKey . entityKey) <$> getBy (TagUName n)

tagNameByID :: Int64 -> DBAction (Maybe TagN)
tagNameByID id = fmap tagName <$> get (toSqlKey id :: Key Tag)


-- ----------------------------------------------------------------------------
-- Operations in the File table
-- ----------------------------------------------------------------------------

addFile :: StorFileN -> DBAction ()
addFile n = insert_ (File $ unS n)

removeFile :: StorFileN -> DBAction ()
removeFile n = do
  fid <- entityKey . fromJust <$> getBy (FileUName $ unS n)
  deleteWhere [LinkFile ==. fid]
  deleteWhere [FileName ==. unS n]

listFiles :: DBAction [StorFileN]
listFiles = map (StorFileN . fileName . entityVal) <$> selectList [] [Asc FileName]

renameFile :: StorFileN -> StorFileN -> DBAction ()
renameFile o n = updateWhere [FileName ==. unS o] [FileName =. unS n]

fileNameByID :: Int64 -> DBAction (Maybe T.Text)
fileNameByID id = fmap fileName <$> get (toSqlKey id :: Key File)

fileIDByName :: T.Text -> DBAction Int64
fileIDByName n = fromSqlKey . entityKey . fromJust <$> getBy (FileUName n)

instance ToStorFileN Int64 where
  toStorFileN id = StorFileN . fromJust <$> runInDB (fileNameByID id)


-- ----------------------------------------------------------------------------
-- Operations in the Preset table
-- ----------------------------------------------------------------------------

addPreset :: TagN -> StorExpr -> DBAction ()
addPreset n e = insert_ $ Preset n e

getPreset :: TagN -> DBAction (Maybe StorExpr)
getPreset n = fmap (presetExpr . entityVal) <$> getBy (PresetUName n)

removePreset :: TagN -> DBAction ()
removePreset n = deleteWhere [PresetName ==. n]

presetIDByName :: TagN -> DBAction (Maybe Int64)
presetIDByName n = fmap (fromSqlKey . entityKey) <$> getBy (PresetUName n)

getPresetByID :: Int64 -> DBAction (Maybe StorExpr)
getPresetByID id = fmap presetExpr <$> get (toSqlKey id :: Key Preset)




