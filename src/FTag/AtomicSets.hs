{-# LANGUAGE RankNTypes, OverloadedStrings, FlexibleContexts #-}

module FTag.AtomicSets where

import Data.Maybe
import Control.Exception (throw)
import Control.Applicative
import qualified Data.Text as T
import FTag.Data
import FTag.DB
import FTag.Expr (resolve)
import FTag.AtomicSets.Operations
import FTag.AtomicSets.SpecialTags
import Env

instance AtomicSet UserAtomicSet where
  getFiles (UASTag     n) = runInDB $ filesForTag n
  getFiles (UASSpecial n) = fromJust <$> resolveSpecial n
  getFiles (UASPreset  n) = runInDB (fromJust <$> getPreset n) >>= resolve
  getFiles (UASByName  n) = do -- TODO: Rewrite this more concisely
    ms <- resolveSpecial n
    case ms of
      (Just fs) -> return fs
      Nothing -> do
        mp <- runInDB $ getPreset n
        case mp of
          (Just exp) -> resolve exp
          Nothing -> runInDB $ filesForTag n -- TODO: fail if the tag doesn't exist

  getFilesPredicate (UASTag     n) p = runInDB $ filesForPred n p
  getFilesPredicate (UASSpecial n) p = fromJust <$> resolveSpecialPred n p
  getFilesPredicate (UASByName  n) p = runInDB $ filesForPred n p 
  getFilesPredicate (UASPreset  n) p = undefined -- TODO: decide what to do here

  testFile (UASSpecial n) f = fromJust <$> testSpecial n f
  testFile (UASTag     n) f = runInDB $ isTagged n f
  testFile (UASByName  n) f = runInDB $ isTagged n f
  testFile (UASPreset  n) f = undefined -- and here


instance AtomicSet StorAtomicSet where
  getFiles (SASTag     id) = runInDB (fromJust <$> filesForTagByID id)
  getFiles (SASSpecial n)  = fromJust <$> resolveSpecial n
  getFiles (SASPreset  id) = runInDB (fromJust <$> getPresetByID id) >>= resolve

  getFilesPredicate (SASTag     id) p = runInDB (fromJust <$> filesForPredByID id p)
  getFilesPredicate (SASSpecial n)  p = fromJust <$> resolveSpecialPred n p
  getFilesPredicate (SASPreset  id) p = undefined -- and here

  testFile (SASTag     id) f = runInDB (fromJust <$> isTaggedByID id f)
  testFile (SASSpecial n)  f = fromJust <$> testSpecial n f
  testFile (SASPreset  id) f = undefined -- and here


