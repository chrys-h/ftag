{-# LANGUAGE RankNTypes, FlexibleContexts #-}

module FTag.Sort (sortByTags) where

import qualified Data.Text as T
import Data.List (sortBy)
import Data.Ord (comparing)

import Env
import FTag.Data
import FTag.DB

sortByTags :: [TagN] -> [StorFileN] -> FTag [StorFileN]
sortByTags ts fs = sortByValues <$> (runInDB (mapM (getValues ts) fs) >>= mapM addUserN)

addUserN :: (StorFileN, [Maybe T.Text]) -> FTag (StorFileN, [Maybe T.Text])
addUserN (f, vs) = let ap uf = (f, vs ++ [Just $ unU uf])
                    in ap <$> toUser f

getValues :: [TagN] -> StorFileN -> DBAction (StorFileN, [Maybe T.Text])
getValues ts f = wrap <$> mapM (\t -> toMaybe <$> isTagged t f) ts
  where toMaybe (FTTaggedWithVal v) = Just v
        toMaybe _                   = Nothing
        wrap vs = (f, vs)

sortByValues :: (Ord v) => [(a, [v])] -> [a]
sortByValues = map fst . sortBy (comparing snd)

