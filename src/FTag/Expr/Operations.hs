{-# LANGUAGE OverloadedStrings, RankNTypes, FlexibleContexts #-}

module FTag.Expr.Operations (resolve, exprToStor) where

import Data.Text as T
import Control.Applicative ((<$>), (*>), (<*), (<*>))
import Control.Monad
import Control.Exception (throw)
import qualified Database.Persist as P
import Database.Esqueleto
import Data.List
import Data.Maybe (fromMaybe, fromJust)

import Env
import FTag.Data
import FTag.DB
import FTag.AtomicSets.Operations

resolve :: (ToStorFileN f, AtomicSet t) => FTGExpr f t -> FTag [StorFileN]
resolve e = nub <$> recurse e
  where
    resSet (FTLitSet fs)     = mapM toStorFileN fs
    resSet (FTAtomic a)      = getFiles a
    resSet (FTPredicate a p) = getFilesPredicate a p
    funOp FTIntersection = intersect
    funOp FTUnion        = union
    funOp FTDifference   = (\\)
    recurse (FTNode o l r) = (funOp o) <$> recurse l <*> recurse r
    recurse (FTLeaf s)     = resSet s

eMapMF :: (Monad m) => (f0 -> m f1) -> FTGExpr f0 t -> m (FTGExpr f1 t)
eMapMF f (FTNode n l r)             = FTNode n <$> eMapMF f l <*> eMapMF f r
eMapMF f (FTLeaf (FTLitSet fs))     = FTLeaf . FTLitSet <$> mapM f fs
eMapMF _ (FTLeaf (FTAtomic a))      = return $ FTLeaf $ FTAtomic a
eMapMF _ (FTLeaf (FTPredicate t p)) = return $ FTLeaf $ FTPredicate t p

eMapMT :: (Monad m) => (t0 -> m t1) -> FTGExpr f t0 -> m (FTGExpr f t1)
eMapMT f (FTNode n l r)             = FTNode n <$> eMapMT f l <*> eMapMT f r
eMapMT f (FTLeaf (FTPredicate t p)) = FTLeaf . flip FTPredicate p <$> f t
eMapMT f (FTLeaf (FTAtomic t))      = FTLeaf . FTAtomic <$> f t
eMapMT _ (FTLeaf (FTLitSet fs))     = return $ FTLeaf $ FTLitSet fs

exprToStor :: UserExpr -> FTag StorExpr
exprToStor e = eMapMT atomicToStor e >>= eMapMF convertFile
  where convertFile f = do fs <- unS <$> toStor f
                           runInDB $ fileIDByName fs




