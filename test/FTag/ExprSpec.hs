{-# LANGUAGE OverloadedStrings #-}

module FTag.ExprSpec where

import Test.Hspec
import FTag (testFTag)
import FTag.Data
import FTag.DB
import FTag.Expr

testTags = [
    ("t1", "f1", Nothing),                        ("t1", "f3", Just "1"),
                           ("t2", "f2", Nothing), ("t2", "f3", Just "2")
  ]

spec :: Spec
spec = do
  describe "Expression parser" $ do
    it "parses expressions in parentheses" $ do
      let e = "tag1 - (tag2 + tag3)"
          r = (FTNode FTDifference
                (FTLeaf $ FTTag "tag1")
                (FTNode FTIntersection
                  (FTLeaf $ FTTag "tag2")
                  (FTLeaf $ FTTag "tag3")))
       in parse e `shouldBe` r

    it "ignores whitespace" $ do
      let e = "   tag1-     (   tag2+  tag3)   "
          r = (FTNode FTDifference
                (FTLeaf $ FTTag "tag1")
                (FTNode FTIntersection
                  (FTLeaf $ FTTag "tag2")
                  (FTLeaf $ FTTag "tag3")))
       in parse e `shouldBe` r

    it "parses names in single quotation marks" $ do
      let e = " 'tag number 1' + 'tag2' "
          r = (FTNode FTIntersection
                (FTLeaf $ FTTag "tag number 1")
                (FTLeaf $ FTTag "tag2"))
       in parse e `shouldBe` r

    it "gives the - operator the most precedence" $ do
      let e = "tag1 - tag2 + tag3"
          r = (FTNode FTIntersection
                (FTNode FTDifference
                  (FTLeaf $ FTTag "tag1")
                  (FTLeaf $ FTTag "tag2"))
                (FTLeaf $ FTTag "tag3"))
       in parse e `shouldBe` r

    it "the - operator is left associative" $ do
      let e = "tag1 - tag2 - tag3 - tag4"
          r = (FTNode FTDifference
                (FTNode FTDifference
                  (FTNode FTDifference
                    (FTLeaf $ FTTag "tag1")
                    (FTLeaf $ FTTag "tag2"))
                  (FTLeaf $ FTTag "tag3"))
                (FTLeaf $ FTTag "tag4"))
       in parse e `shouldBe` r

    it "gives the , operator the least precedence" $ do
      let e = "tag1, tag2 + tag3"
          r = (FTNode FTUnion
                (FTLeaf $ FTTag "tag1")
                (FTNode FTIntersection
                  (FTLeaf $ FTTag "tag2")
                  (FTLeaf $ FTTag "tag3")))
       in parse e `shouldBe` r

    it "parses literal sets" $ do
      let e = "tag1, {f1, f2}"
          r = (FTNode FTUnion
                (FTLeaf $ FTTag "tag1")
                (FTLeaf $ FTLitSet $ map UserFileN ["f1", "f2"]))
       in parse e `shouldBe` r

    it "parses predicates" $ do
      let e = "[t1 != val]"
          r = FTLeaf $ FTPredicate "t1" $ FTNotEqual "val"
       in parse e `shouldBe` r

    it "parses predicates with sets in them" $ do
      let e = "[t1 in {v1, v2, v3}]"
          r = FTLeaf $ FTPredicate "t1" $ FTInSet ["v1", "v2", "v3"]
       in parse e `shouldBe` r

  describe "Syntactic sugar" $ do
    it "fills out blank operators with intersection operations" $ do
      let e = "t1 t2, t3 t4"
          r = (FTNode FTUnion
                (FTNode FTIntersection
                  (FTLeaf $ FTTag "t1")
                  (FTLeaf $ FTTag "t2"))
                (FTNode FTIntersection
                  (FTLeaf $ FTTag "t3")
                  (FTLeaf $ FTTag "t4")))
       in parse e `shouldBe` r

    it "fills out blank difference operands with the 'all' set" $ do
      let e = "-t1"
          r = (FTNode FTDifference
                (FTLeaf $ FTTag "all")
                (FTLeaf $ FTTag "t1"))
       in parse e `shouldBe` r


