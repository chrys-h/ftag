{-# LANGUAGE OverloadedStrings #-}

module FTag.DBSpec where

import Test.Hspec
import FTag (testFTag)
import FTag.Data
import FTag.DB

testTags = [
    ("t1", "f1", Nothing),                        ("t1", "f3", Just "1"),
                           ("t2", "f2", Nothing), ("t2", "f3", Just "2")
  ]

spec :: Spec
spec = do
  describe "Adding a tag to a file" $ do
    it "fails if the file is already tagged" $ do
      testFTag testTags (tagFile "t1" (StorFileN "f1") Nothing)
        `shouldThrow` (== FTFileAlreadyTagged "t1" (StorFileN "f1") Nothing)

    it "fails if the file is unknown" $ do
      testFTag testTags (tagFile "t2" (StorFileN "f0") Nothing)
        `shouldThrow` (== FTFileUnknown (StorFileN "f0"))

    it "fails if the tag is unknown" $ do
      testFTag testTags (tagFile "t0" (StorFileN "f1") Nothing)
        `shouldThrow` (== FTNoSuchTag "t0")

    it "works otherwise" $ do
      testFTag testTags (tagFile "t1" (StorFileN "f2") Nothing >> isTagged "t1" (StorFileN "f2"))
        `shouldReturn` FTTaggedNoVal

    it "sets the value if one is given" $ do
      testFTag testTags (tagFile "t1" (StorFileN "f2") (Just "1") >> isTagged "t1" (StorFileN "f2"))
        `shouldReturn` FTTaggedWithVal "1"

  describe "Removing a tag from a file" $ do
    it "fails if the file is unknown" $ do
      testFTag testTags (untagFile "t1" (StorFileN "f0"))
        `shouldThrow` (== FTFileUnknown (StorFileN "f0"))

    it "fails if the tag is unknown" $ do
      testFTag testTags (untagFile "t0" (StorFileN "f2"))
        `shouldThrow` (== FTNoSuchTag "t0")

    it "fails if the file is not tagged" $ do
      testFTag testTags (untagFile "t1" (StorFileN "f2"))
        `shouldThrow` (== FTFileNotTagged "t1" (StorFileN "f2"))

    it "works otherwise" $ do
      testFTag testTags (untagFile "t1" (StorFileN "f1") >> isTagged "t1" (StorFileN "f1"))
        `shouldReturn` FTNotTagged

  describe "Listing tags on a file" $ do
    it "fails if the file is unknown" $ do
      testFTag testTags (tagsForFile (StorFileN "f0")) `shouldThrow` (== FTFileUnknown (StorFileN "f0"))

    it "works otherwise" $ do
      testFTag testTags (tagsForFile (StorFileN "f3")) `shouldReturn` [ "t1", "t2" ] 

  describe "Checking if a file is tagged" $ do
    it "fails if the tag is unknown" $ do
      testFTag testTags (isTagged "t0" (StorFileN "f1")) `shouldThrow` (== FTNoSuchTag "t0")

    it "fails if the file is unknown" $ do
      testFTag testTags (isTagged "t1" (StorFileN "f0")) `shouldThrow` (== FTFileUnknown (StorFileN "f0"))

    it "returns FTNotTagged if the file is not tagged" $ do
      testFTag testTags (isTagged "t2" (StorFileN "f1")) `shouldReturn` FTNotTagged

    it "returns FTTaggedNoVal if the file is tagged with no value" $ do
      testFTag testTags (isTagged "t1" (StorFileN "f1")) `shouldReturn` FTTaggedNoVal

    it "returns FTTaggedWithVal if the file is tagged with the value" $ do
      testFTag testTags (isTagged "t1" (StorFileN "f3")) `shouldReturn` FTTaggedWithVal "1"

  describe "Renaming a file" $ do
    it "fails if the file is unknown" $ do
      testFTag testTags (renameFile (StorFileN "f0") (StorFileN "f4"))
        `shouldThrow` (== FTFileUnknown (StorFileN "f0"))

    it "fails if the new name is already taken" $ do
      testFTag testTags (renameFile (StorFileN "f1") (StorFileN "f2"))
        `shouldThrow` (== FTFileAlreadyKnown (StorFileN "f2"))

    it "works otherwise" $ do
      testFTag testTags (renameFile (StorFileN "f1") (StorFileN "f4") >> listFiles)
        `shouldReturn` map StorFileN [ "f2", "f3", "f4" ]

  describe "Renaming a tag" $ do
    it "fails if the tag doesn't exist" $ do
      testFTag testTags (renameTag "t0" "t4") `shouldThrow` (== FTNoSuchTag "t0")

    it "fails if the new name is already taken" $ do
      testFTag testTags (renameTag "t1" "t2") `shouldThrow` (== FTTagAlreadyExists "t2")

    it "works otherwise" $ do
      testFTag testTags (renameTag "t1" "t3" >> listTags) `shouldReturn` ["t2", "t3" ]

  describe "Listing all values of a tag" $ do
    it "fails if the tag doesn't exist" $ do
      testFTag testTags (listVals "t0") `shouldThrow` (== FTNoSuchTag "t0")

    it "returns all values of the tag if it has any" $ do
      testFTag testTags (listVals "t2") `shouldReturn` ["2"]






